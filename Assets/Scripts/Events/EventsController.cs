﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsController : Singleton<EventsController>{
    private bool isEventActive = false;

    public Enemy meteor;
    public Animator anim;
    private Vector2 startSpeed;

    public void startEvent(Card.CardIdentity cardIdentity) {
        if (!isEventActive) {
            switch (cardIdentity) {
                case Card.CardIdentity.EVENT_MASSIVE_ATTACK:
                    StartCoroutine(cr_start_massive_attack());

                    break;
                case Card.CardIdentity.EVENT_METEORS:
                    foreach (Spawner sp in EnemiesSpawner.Instance.spawners) {
                        sp.spawnEnemy(meteor);
                    }

                    break;
                case Card.CardIdentity.EVENT_PROBLEMS_ON_THE_SHIP:
                    Player pl = FindObjectOfType<Player>();

                    if (pl != null) {
                        isEventActive = true;
                        pl.currentWeapon.isCanShoot = false;
                        startSpeed = pl.speed;
                        pl.speed = new Vector2(pl.speed.x / 2, pl.speed.y / 2);
                        anim.SetBool("eventIsActive", true);

                        StartCoroutine(cr_wait_event_ship_problems_over(pl));
                    }

                    break;
            }
        }
    }

    private IEnumerator cr_start_massive_attack() {
        foreach (Spawner sp in EnemiesSpawner.Instance.spawners) {
            sp.spawnEnemy();
        }
        yield return new WaitForSeconds(10f);
        foreach (Spawner sp in EnemiesSpawner.Instance.spawners) {
            sp.spawnEnemy();
        }
        yield return new WaitForSeconds(10f);
        foreach (Spawner sp in EnemiesSpawner.Instance.spawners) {
            sp.spawnEnemy();
        }
        yield return new WaitForSeconds(10f);
    }

    private IEnumerator cr_wait_event_ship_problems_over(Player pl) {
        yield return new WaitForSeconds(15f);

        if (pl != null) {
            isEventActive = false;
            pl.currentWeapon.isCanShoot = true;
            pl.speed = startSpeed;
            anim.SetBool("eventIsActive", false);
        }
    }
}