﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundGenerator : MonoBehaviour {

    public static BackGroundGenerator instance;

    public int maxSmallStars = 50;
    public int maxMiddleStars = 25;
    public int maxBigStars = 10;

    public Star smallStar;
    public Star middleStar;
    public Star bigStar;

    public Collider2D starsGenerationZone;

    private IEnumerator cr_generate_small_stars;
    private IEnumerator cr_generate_middle_stars;
    private IEnumerator cr_generate_big_stars;

	void Awake () {
        instance = this;

        cr_generate_small_stars = cr_star_generator(smallStar, maxSmallStars, 0.1f, 0.25f);
        cr_generate_middle_stars = cr_star_generator(middleStar, maxSmallStars, 1f, 1.5f);
        cr_generate_big_stars = cr_star_generator(bigStar, maxSmallStars, 2f, 2.5f);

        StartCoroutine(cr_generate_small_stars);
        StartCoroutine(cr_generate_middle_stars);
        StartCoroutine(cr_generate_big_stars);
    }

    private IEnumerator cr_star_generator(Star go, int counter, float min, float max) {
        while (true) {
            yield return new WaitForSeconds(Random.Range(min, max));

            if (FindObjectsOfType<SmallStar>().Length < counter) {
                GameObject tmpGo = Instantiate(go.gameObject, rndPosition(starsGenerationZone), Quaternion.identity);
                tmpGo.transform.SetParent(transform);
            }
        }
    }

    private Vector2 rndPosition(Collider2D colliderToSelectPlace) {
        Vector2 randomPosition = new Vector2(colliderToSelectPlace.transform.position.x +
                                                     colliderToSelectPlace.offset.x +
                                                     Random.Range(colliderToSelectPlace.bounds.min.x,
                                                     colliderToSelectPlace.bounds.max.x),
                                                     colliderToSelectPlace.transform.position.y +
                                                     colliderToSelectPlace.offset.y +
                                                     Random.Range(colliderToSelectPlace.bounds.min.y,
                                                     colliderToSelectPlace.bounds.max.y));

        return randomPosition;
    }

    private void OnDestroy() {
        StopCoroutine(cr_generate_small_stars);
        StopCoroutine(cr_generate_middle_stars);
        StopCoroutine(cr_generate_big_stars);
    }
}
