﻿using UnityEngine;
using System.Runtime.InteropServices;

public class Link : MonoBehaviour {
    public void OpenLinkJSPlugin() {
#if !UNITY_EDITOR
        openWindow("https://twitter.com/Serious_07");
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}