﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarsDestroyer : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.GetComponent<Star>()) {
            Destroy(collision.gameObject);
        }
    }
}
