﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenVictory : Screen {
    public void onBtnExitToMainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");

        PlaySound();
    }
}
