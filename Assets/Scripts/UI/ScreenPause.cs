﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenPause : Screen {
    public void onBtnResume() {
        Time.timeScale = 1f;
        gameObject.SetActive(false);

        PlaySound();
    }

    public void onBtnExitToMainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");

        PlaySound();
    }
}
