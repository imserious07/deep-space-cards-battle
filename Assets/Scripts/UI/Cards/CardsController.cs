﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsController : Singleton<CardsController> {
    public Canvas canvas;

    public List<RectTransform> spawnPos;
    public List<Card> allCards;

    private List<Card> currentCards;

    public Sprite secretEventCardSprite;

    public Sprite meteors_spr;
    public Sprite problems_spr;
    public Sprite attack_spr;

    public Text cardsTitle;

    public AudioSource aidioSource;

    // Use this for initialization
	void Awake () {
        currentCards = new List<Card>();
    }

    [ContextMenu("Spawn event cards")]
    public void SpawnEventCards() {
        SpawnCards(Card.CardType.GAME_EVENT);
    }

    [ContextMenu("Spawn weapon cards")]
    public void SpawnWeaponCards() {
        SpawnCards(Card.CardType.WEAPON);
    }

    [ContextMenu("Clear cards")]
    public void ClearCardsTest() {
        ClearCards();
    }

    public void ShowHiddenCards() {
        foreach (Card card in currentCards) {
            if (card.cardIdentity == Card.CardIdentity.EVENT_MASSIVE_ATTACK) {
                card.description.text = "Massive attack";
                if(attack_spr != null)
                    card.image.sprite = attack_spr;
            }
            if (card.cardIdentity == Card.CardIdentity.EVENT_PROBLEMS_ON_THE_SHIP) {
                card.description.text = "Problems on ship";
                if (problems_spr != null)
                    card.image.sprite = problems_spr;
            }
            if (card.cardIdentity == Card.CardIdentity.EVENT_METEORS) {
                card.description.text = "Meteors";
                if (meteors_spr != null)
                    card.image.sprite = meteors_spr;
            }
        }
    }

    public void ClearCards() {
        foreach (RectTransform pos in spawnPos) {
            for (int i = 0; i < pos.transform.childCount; i++) {
                Destroy(pos.transform.GetChild(i).gameObject);
            }
        }

        currentCards.Clear();

        cardsTitle.text = "";

        Time.timeScale = 1.0f;
    }

    public void SpawnCards(Card.CardType type) {
        ClearCards();

        Time.timeScale = 0.0f;

        switch (type) {
            case Card.CardType.GAME_EVENT:
                cardsTitle.text = "PICK EVENT CARD";

                foreach (RectTransform pos in spawnPos) {
                    Card rndCard = GetRandomCardFromPull(currentCards, Card.CardType.GAME_EVENT);
                    GameObject goCard = Instantiate(rndCard.gameObject, pos.transform);
                    goCard.GetComponent<RectTransform>().localPosition = Vector3.zero;
                    currentCards.Add(rndCard);
                }

                foreach (Card card in currentCards) {
                    //if(card.description != null)
                    //    card.description.text = "???";
                    //if(secretEventCardSprite != null && card.image != null)
                    //    card.image.sprite = secretEventCardSprite;
                }

                break;
            case Card.CardType.WEAPON:
                cardsTitle.text = "PICK WEAPON CARD";

                foreach (RectTransform pos in spawnPos) {
                    Card rndCard = GetRandomCardFromPull(currentCards, Card.CardType.WEAPON);
                    GameObject goCard = Instantiate(rndCard.gameObject, pos.transform);
                    goCard.GetComponent<RectTransform>().localPosition = Vector3.zero;
                    currentCards.Add(rndCard);
                }

                break;
        }
    }

    private Card GetCardByIdentity(Card.CardIdentity indentity) {
        foreach (Card card in allCards) {
            if (card.cardIdentity.Equals(indentity)) {
                return card;
            }
        }

        return null;
    }

    private Card GetRandomCardFromPull(List<Card> cards, Card.CardType type) {
        List<Card> allCardsOfOneType = new List<Card>();

        foreach (Card card in allCards) {
            if(card.cardType.Equals(type))
                allCardsOfOneType.Add(card);
        }

        int randomCard = -1;

        if (allCardsOfOneType.Count <= cards.Count) {
            return null;
        }

        while (true) {
            randomCard = Random.Range(0, allCardsOfOneType.Count);

            if (!cards.Contains(allCardsOfOneType[randomCard])) {
                return allCardsOfOneType[randomCard];
            }
        }
    }
}