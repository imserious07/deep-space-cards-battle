﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {
    public CardType cardType;
    public CardIdentity cardIdentity;
    public Image image;
    public Text description;
    public AudioClip clip;
    public AudioSource source;

    public Button button;

    public enum CardType {
        GAME_EVENT,
        WEAPON
    }

    public enum CardIdentity {
        WEAPON_BEAM_DISK,
        WEAPON_BEAM_MACHINE_GUN,
        WEAPON_BEAM_SPREAD_GUN,
        WEAPON_MASSIVE_ATTACK,
        WEAPON_POWER_SHIELD,
        WEAPON_ROCKETS,
        EVENT_METEORS,
        EVENT_MASSIVE_ATTACK,
        EVENT_PROBLEMS_ON_THE_SHIP
    }

	void Awake () {
        if(button == null)
            button = GetComponent<Button>();

        if (image = null)
            image = GetComponent<Image>();

        button.onClick.AddListener(OnButonClick);

        if (source == null)
            source = CardsController.Instance.aidioSource;
    }

    private IEnumerator cr_Show_cards() {
        CardsController.Instance.ShowHiddenCards();
        Time.timeScale = 0.01f;
        yield return new WaitForSeconds(0.01f);
        CardsController.Instance.ClearCards();
        EventsController.Instance.startEvent(cardIdentity);
    }

    private void OnButonClick() {
        switch (cardType) {
            case CardType.WEAPON:
                print("Clicked on card!");
                FindObjectOfType<Player>().selectSpecificWeaponForPlayer(cardIdentity);
                CardsController.Instance.ClearCards();
                
                break;
            case CardType.GAME_EVENT:
                print("Clicked on card!");
                CardsController.Instance.ClearCards();
                EventsController.Instance.startEvent(cardIdentity);
                break;
        }

        source.clip = clip;
        source.Play();
    }
}
