﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screen : MonoBehaviour {
    public AudioClip audioClip;
    public AudioSource audioSource;

    public void Awake() {
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound() {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
}
