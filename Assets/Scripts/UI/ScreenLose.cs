﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenLose : Screen {
    public void onBtnExitToMainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");

        PlaySound();
    }

    public void onBtnTryAgain() {
        Time.timeScale = 1f;

        foreach (Meteor m in FindObjectsOfType<Meteor>()) {
            m.canSpawn = false;
        }

        SceneManager.LoadScene("Game");

        PlaySound();
    }
}
