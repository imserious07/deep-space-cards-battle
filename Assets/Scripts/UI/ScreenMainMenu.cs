﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenMainMenu : Screen {
    public void Start() {
        Time.timeScale = 1f;
    }

    public void onBtnExitGame() {
        PlaySound();
        StartCoroutine(cr_WaitForExit());
    }

    IEnumerator cr_WaitForExit() {
        yield return new WaitForSeconds(2f);
        // save any game data here
        #if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    public void onBtnPlayGame() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");

        PlaySound();
    }

    public void onBtnTwitter() {
        Application.OpenURL("https://twitter.com/Serious_07");

        PlaySound();
    }

    public void onBtnLudumDare() {
        Application.OpenURL("https://ldjam.com/");

        PlaySound();
    }
}
