﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBeamSpreadGun : MonoBehaviour, iWeapon {
    public float _shootingInterval = 1f;
    private bool _isShooting = false;
    private IEnumerator _cr_shooting_progress;
    private bool _isEnemyWeapon = false;
    private bool _isCanShoot = true;
    public Bullet _bullet;
    public AudioSource _audioSourse;
    public AudioClip _audioClip;

    public float shootingInterval {
        get {
            return _shootingInterval;
        }
        set {
            _shootingInterval = value;
        }
    }

    public bool isShooting {
        get {
            return _isShooting;
        }

        set {
            _isShooting = value;
        }
    }

    public Bullet bulletType {
        get {
            return _bullet;
        }

        set {
            _bullet = value;
        }
    }

    public IEnumerator cr_shooting_progress {
        get {
            return _cr_shooting_progress;
        }

        set {
            _cr_shooting_progress = value;
        }
    }

    public bool isEnemyWeapon {
        get {
            return _isEnemyWeapon;
        }

        set {
            _isEnemyWeapon = value;
        }
    }

    public bool isCanShoot {
        get {
            return _isCanShoot;
        }

        set {
            _isCanShoot = value;
        }
    }

    public AudioSource audioSource {
        get {
            return _audioSourse;
        }

        set {
            _audioSourse = value;
        }
    }

    public AudioClip audioClip {
        get {
            return _audioClip;
        }
        set {
            _audioClip = value;
        }
    }

    public void Start() {
        cr_shooting_progress = cr_bullet_creator();
        StartCoroutine(cr_shooting_progress);

        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();
    }

    public void OnDestroy() {
        StopCoroutine(cr_shooting_progress);
    }

    public void createBullet() {
        GameObject go = Instantiate(bulletType.gameObject, transform.position, Quaternion.Euler(0, 0, 135));
        go.GetComponent<BulletBeamSpreadGun>().isPlayerBullet = !isEnemyWeapon;
        go = Instantiate(bulletType.gameObject, transform.position, Quaternion.Euler(0, 0, 90));
        go.GetComponent<BulletBeamSpreadGun>().isPlayerBullet = !isEnemyWeapon;
        go = Instantiate(bulletType.gameObject, transform.position, Quaternion.Euler(0, 0, 45));
        go.GetComponent<BulletBeamSpreadGun>().isPlayerBullet = !isEnemyWeapon;

        if (audioSource != null && audioClip != null) {
            audioSource.clip = audioClip;
            audioSource.Play();
        }
    }

    public IEnumerator cr_bullet_creator() {
        while (true) {
            if (isShooting && isCanShoot) {
                createBullet();
            }
            yield return new WaitForSeconds(shootingInterval);
        }
    }
}
