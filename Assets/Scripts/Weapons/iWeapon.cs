﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iWeapon {
    Bullet bulletType { get; set; }
    float shootingInterval { get; set; }
    bool isShooting { get; set; }
    bool isEnemyWeapon { get; set; }
    bool isCanShoot { get; set; }
    IEnumerator cr_shooting_progress { get; set; }
    AudioSource audioSource {get; set;}
    AudioClip audioClip { get; set; }

    void createBullet();
    IEnumerator cr_bullet_creator();
}