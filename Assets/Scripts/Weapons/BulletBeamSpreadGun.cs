﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBeamSpreadGun : Bullet {
    public override bool isPlayerBullet {
        get {
            return _isPlayerBullet;
        }
        set {
            _isPlayerBullet = value;

            if (_isPlayerBullet) {
                UpdateDirections();

                gameObject.layer = LayerMask.NameToLayer("PlayerBullet");
            } else {
                speed = new Vector2(0f, -10f);
                gameObject.layer = LayerMask.NameToLayer("EnemyBullet");
            }
            moveDirection = speed;
        }
    }

    public void UpdateDirections() {
        if (transform.rotation == Quaternion.Euler(0, 0, 135))
            speed = new Vector2(-10f, 10f);
        if (transform.rotation == Quaternion.Euler(0, 0, 90))
            speed = new Vector2(0f, 10f);
        if (transform.rotation == Quaternion.Euler(0, 0, 45))
            speed = new Vector2(10f, 10f);

        moveDirection = speed;
    }
}
