﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPowerShield : Bullet {
    public override bool isPlayerBullet {
        get {
            return base.isPlayerBullet;
        }

        set {
            base.isPlayerBullet = value;

            speed = Vector2.zero;
            moveDirection = Vector2.zero;
            FindObjectOfType<Player>().hp.isCanGetDmg = false;
        }
    }

    public override void FixedUpdate() {
        base.FixedUpdate();

        transform.position = FindObjectOfType<Player>().transform.position;
    }

    public void OnDestroy() {
        WeaponPowerShield powerShield = FindObjectOfType<WeaponPowerShield>();

        if(powerShield != null && powerShield.powerShield != null)
            powerShield.powerShield = null;
    }

    public override void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.GetComponent<Enemy>() || collision.collider.GetComponent<Bullet>()) {
            base.OnCollisionEnter2D(collision);

            FindObjectOfType<Player>().hp.isCanGetDmg = true;
            Destroy(gameObject);
        }
    }
}
