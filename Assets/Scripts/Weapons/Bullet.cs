﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Entity {
    public bool _isPlayerBullet;

    public virtual bool isPlayerBullet {
        get {
            return _isPlayerBullet;
        }
        set {
            _isPlayerBullet = value;

            if (_isPlayerBullet) {
                speed = new Vector2(0f, 10f);
                gameObject.layer = LayerMask.NameToLayer("PlayerBullet");
            } else {
                speed = new Vector2(0f, -10f);
                gameObject.layer = LayerMask.NameToLayer("EnemyBullet");
            }
            moveDirection = speed;
        }
    }

    public virtual void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.GetComponent<Border>())
            Destroy(gameObject);
        Player pl = collision.gameObject.GetComponent<Player>();

        if (pl != null && gameObject.layer.Equals(LayerMask.NameToLayer("EnemyBullet")) && !pl.isDead) {
            pl.hp.hpPoints--;
            Destroy(gameObject);
        }

        Enemy en = collision.gameObject.GetComponent<Enemy>();

        if (en != null && gameObject.layer.Equals(LayerMask.NameToLayer("PlayerBullet")) && !en.isDead) {
            en.hp.hpPoints--;
            Destroy(gameObject);
        }

        Meteor mt = collision.gameObject.GetComponent<Meteor>();

        if (mt != null && gameObject.layer.Equals(LayerMask.NameToLayer("PlayerBullet")) && !mt.isDead) {
            mt.hp.hpPoints--;
            Destroy(gameObject);
        }
    }
}
