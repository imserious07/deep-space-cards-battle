﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBeamDisk : Bullet {
    public override void FixedUpdate() {
        base.FixedUpdate();
        transform.Rotate(Vector3.forward, 5f);
    }
}
