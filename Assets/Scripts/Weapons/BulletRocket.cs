﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRocket : Bullet {
    public override bool isPlayerBullet {
        get {
            return base.isPlayerBullet;
        }

        set {
            base.isPlayerBullet = value;
            speed = new Vector2(speed.x, speed.x);
        }
    }

    public override void FixedUpdate() {
        Enemy en = closesedEnemy();

        if (en != null) {
            Vector3 difference = en.transform.position - transform.position;

            moveDirection = difference;

            difference.Normalize();

            float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rotation_z + 0.0f);
        }

        base.FixedUpdate();
    }

    public Enemy closesedEnemy() {
        Enemy result = null;
        List<Enemy> enemies = new List<Enemy>(FindObjectsOfType<Enemy>());

        float maxDist = float.MaxValue;

        foreach (Enemy en in enemies) {
            float dist = Vector2.Distance((Vector2)transform.position,
                (Vector2)en.transform.position);

            if (dist < maxDist) {
                result = en;
                maxDist = dist;
            }
        }

        return result;
    }
}