﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Enemy {
    public MeteorType meteorType;

    public Meteor middleMeteor;
    public Meteor smallMeteor;

    private int rnd;
    public bool canSpawn = true;

    public enum MeteorType {
        SMALL,
        MIDDLE,
        BIG
    }

    public override void Start() {
        rnd = Random.Range(1, 2);
        base.Start();
    }

    public override void FixedUpdate() {
        base.FixedUpdate();

        switch (rnd) {
            case 0:
                transform.Rotate(Vector3.forward, 2);
                break;
            case 1:
                transform.Rotate(Vector3.forward, -2);
                break;
        }
    }

    public void OnApplicationQuit() {
        canSpawn = false;
    }

    public void OnDestroy() {
        if (canSpawn) {
            switch (meteorType) {
                case MeteorType.MIDDLE:
                    for (int i = 0; i < 2; i++) {
                        Instantiate(smallMeteor, transform.position, Random.rotation);
                    }

                    break;
                case MeteorType.BIG:
                    for (int i = 0; i < 2; i++) {
                        Instantiate(middleMeteor, transform.position, Random.rotation);
                    }

                    break;
            }
        }
    }

    public override void PlayDestroyEffect() {
        if (explosion != null) {
            GameObject go_exp = Instantiate(explosion, transform);
            go_exp.transform.localPosition = Vector3.zero;
        }

        isDead = true;
        Destroy(gameObject);
    }
}
