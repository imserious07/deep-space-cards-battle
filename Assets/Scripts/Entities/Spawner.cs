﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public List<Enemy> enemies;

    public void spawnEnemy() {
        if (enemies != null && enemies.Count > 0) {
            int rndEnemy = Random.Range(0, enemies.Count);

            GameObject enemy = Instantiate(enemies[rndEnemy].gameObject, transform);
            enemy.transform.localPosition = Vector3.zero;
        }
    }

    public void spawnEnemy(Enemy enemy) {
        GameObject enemyGO = Instantiate(enemy.gameObject, transform);
        enemy.transform.localPosition = Vector3.zero;
    }
}
