﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {
    private Vector2 _moveDirection;

    public Vector2 moveDirection {
        set {
            _moveDirection = value;
            if (!isDead) {
                rb.velocity = _moveDirection;
            } else {
                rb.velocity = Vector3.zero;
            }
        }
        get {
            return _moveDirection;
        }
    }
    public Vector2 speed = new Vector2(10, 5);

    public HP hp;
    public Rigidbody2D rb;

    public GameObject explosion;
    public bool isDead = false;

    private void Awake() {
        if(rb == null) rb = GetComponent<Rigidbody2D>();
        if(hp == null) hp = GetComponent<HP>();

        if (!isDead)
            rb.velocity = moveDirection;
    }

    public virtual void FixedUpdate() {
        // rb.AddForce(moveDirection, ForceMode2D.Force);

        if (!isDead)
            rb.velocity = moveDirection;
    }

    public virtual void PlayDestroyEffect() {
        gameObject.layer = LayerMask.NameToLayer("Star");

        if (explosion != null) {
            GameObject go_exp = Instantiate(explosion, transform);
            go_exp.transform.localPosition = Vector3.zero;
        }

        isDead = true;

        StartCoroutine(cr_RemoveEntity());
    }

    private IEnumerator cr_RemoveEntity() {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }
}
