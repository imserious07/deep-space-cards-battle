﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Entity {
    public EnemyPatterns currentAIBehaivor;

    public iWeapon currentWeapon;

    public enum EnemyPatterns {
        NONE,
        DIAGONAL_UP_RIGHT,
        DIAGONAL_UP_LEFT,
        DIAGONAL_UP_DOWN,
        DIAGONAL_LEFT_RIGHT,
        DIAGONAL_RIGHT_LEFT,
        CIRCLE_LEFT_RIGHT,
        CIRCLE_RIGHT_LEFT,
        WAVE_LEFT_RIGHT,
        WAVE_RIGHT_LEFT,
        TOP_RIGHT,
        TOP_LEFT,
        RIGHT_TOP,
        LEFT_TOP,
        RANDOM,
        TO_CENTER
    }

    public void Awake() {
        currentWeapon = GetComponent<iWeapon>();

        if (currentWeapon != null) {
            currentWeapon.isShooting = true;
            currentWeapon.isEnemyWeapon = true;
        }

        moveDirection = Vector2.zero;
    }

    public virtual void Start() {
        switch (currentAIBehaivor) {
            case EnemyPatterns.DIAGONAL_UP_RIGHT:
                moveDirection = (Vector2.down + Vector2.right) * speed.x;
                break;
            case EnemyPatterns.DIAGONAL_UP_LEFT:
                moveDirection = (Vector2.down + Vector2.left) * speed.x;
                break;
            case EnemyPatterns.DIAGONAL_UP_DOWN:
                moveDirection = Vector2.down * speed.x;
                break;
            case EnemyPatterns.DIAGONAL_LEFT_RIGHT:
                moveDirection = Vector2.right * speed.x;
                break;
            case EnemyPatterns.DIAGONAL_RIGHT_LEFT:
                moveDirection = Vector2.left * speed.x;
                break;
            case EnemyPatterns.TOP_RIGHT:
                moveDirection = Vector2.down * speed.x;
                StartCoroutine(cr_changeDirection(Vector2.right * speed, 2f, 3.5f));
                break;
            case EnemyPatterns.TOP_LEFT:
                moveDirection = Vector2.down * speed.x;
                StartCoroutine(cr_changeDirection(Vector2.left * speed, 2f, 3.5f));
                break;
            case EnemyPatterns.LEFT_TOP:
                moveDirection = Vector2.left * speed.x;
                StartCoroutine(cr_changeDirection(Vector2.up * speed, 2f, 3.5f));
                break;
            case EnemyPatterns.RIGHT_TOP:
                moveDirection = Vector2.right * speed.x;
                StartCoroutine(cr_changeDirection(Vector2.up * speed, 2f, 3.5f));
                break;
            case EnemyPatterns.RANDOM:
                Vector2 randomVector = new Vector2(Random.value, Random.value);

                randomVector.Normalize();
                moveDirection = randomVector * speed.x;
                break;
            case EnemyPatterns.TO_CENTER:
                moveDirection = Vector2.zero - (Vector2)transform.position;
                break;
        }
    }

    public IEnumerator cr_changeDirection(Vector2 newDirection, float waitMin, float waitMax) {
        yield return new WaitForSeconds(Random.Range(waitMin, waitMax));
        moveDirection = newDirection;
    }
}
