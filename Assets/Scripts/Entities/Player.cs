﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Entity {
    public List<iWeapon> allWeapons;
    public iWeapon currentWeapon;
    public ScreenLose loseScreen;
    public ScreenPause pauseScreen;

    // Update is called once per frame
    void Update() {
        moveDirection = new Vector2(Input.GetAxis("Horizontal") * speed.x,
                                    Input.GetAxis("Vertical") * speed.y);

        ReadInput();
    }

    private void Start() {
        Init();

        currentWeapon = selectRandomWeaponForPlayer();
        currentWeapon.isEnemyWeapon = false;
    }

    private void Init() {
        allWeapons = new List<iWeapon>(GetComponents<iWeapon>());
    }

    public iWeapon selectRandomWeaponForPlayer() {
        int rndWeaponNumber = Random.Range(0, allWeapons.Count);
        iWeapon rndWeapon = allWeapons[rndWeaponNumber];

        return rndWeapon;
    }

    public void selectSpecificWeaponForPlayer(iWeapon weaponType) {
        if (weaponType is WeaponBeamDisk) {
            currentWeapon = gameObject.GetComponent<WeaponBeamDisk>();
        } else if (weaponType is WeaponBeamMachineGun) {
            currentWeapon = gameObject.GetComponent<WeaponBeamMachineGun>();
        } else if (weaponType is WeaponPowerShield) {
            currentWeapon = gameObject.GetComponent<WeaponPowerShield>();
        } else if (weaponType is WeaponRockets) {
            currentWeapon = gameObject.GetComponent<WeaponRockets>();
        } else if (weaponType is WeaponBeamSpreadGun) {
            currentWeapon = gameObject.GetComponent<WeaponBeamSpreadGun>();
        }

        currentWeapon.isEnemyWeapon = false;
    }

    public void selectSpecificWeaponForPlayer(Card.CardIdentity weaponType) {
        currentWeapon.isShooting = false;

        switch (weaponType) {
            case Card.CardIdentity.WEAPON_BEAM_DISK:
                currentWeapon = gameObject.GetComponent<WeaponBeamDisk>();
                currentWeapon.isEnemyWeapon = false;
                break;
            case Card.CardIdentity.WEAPON_BEAM_MACHINE_GUN:
                currentWeapon = gameObject.GetComponent<WeaponBeamMachineGun>();
                currentWeapon.isEnemyWeapon = false;
                break;
            case Card.CardIdentity.WEAPON_BEAM_SPREAD_GUN:
                currentWeapon = gameObject.GetComponent<WeaponBeamSpreadGun>();
                currentWeapon.isEnemyWeapon = false;
                break;
            case Card.CardIdentity.WEAPON_POWER_SHIELD:
                currentWeapon = gameObject.GetComponent<WeaponPowerShield>();
                currentWeapon.isEnemyWeapon = false;
                break;
            case Card.CardIdentity.WEAPON_ROCKETS:
                currentWeapon = gameObject.GetComponent<WeaponRockets>();
                currentWeapon.isEnemyWeapon = false;
                break;
        }
    }

    private void ReadInput() {
        if (!isDead) {
            if (Input.GetButtonDown("Shoot")) {
                currentWeapon.isShooting = true;
            }
            if (Input.GetButtonUp("Shoot")) {
                currentWeapon.isShooting = false;
            }
            if (Input.GetButtonDown("Escape")) {
                pauseScreen.gameObject.SetActive(true);
                Time.timeScale = 0f;
            }
        } else {
            currentWeapon.isShooting = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        print("Collision");

        if (collision.collider.GetComponent<Enemy>()) {
            collision.collider.GetComponent<Enemy>().hp.hpPoints--;
            hp.hpPoints--;
        }
    }

    private void OnDestroy() {
        Time.timeScale = 0f;
        if(loseScreen != null)
        loseScreen.gameObject.SetActive(true);
    }
}
