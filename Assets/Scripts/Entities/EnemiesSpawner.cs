﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesSpawner : Singleton<EnemiesSpawner> {
    public List<Spawner> spawners;
    public Slider slider;
    public ScreenVictory screenVictory;

    public float minSpawnInterval = 0.5f;
    public float maxSpawnInterval = 1.2f;

    public void Start() {
        StartCoroutine(cr_spawn_enemy());
        StartCoroutine(cr_change_difficulty());
        StartCoroutine(cr_change_progress());

        spawners = new List<Spawner>(GetComponentsInChildren<Spawner>());
    }

    private IEnumerator cr_spawn_enemy() {
        while (true) {
            if (spawners != null && spawners.Count > 0) {
                int rndSpawner = Random.Range(0, spawners.Count);

                spawners[rndSpawner].spawnEnemy();
            }
            yield return new WaitForSeconds(Random.Range(minSpawnInterval, maxSpawnInterval));
        }
    }

    private IEnumerator cr_change_difficulty() {
        CardsController.Instance.SpawnWeaponCards();

        while (minSpawnInterval > 0) {
            yield return new WaitForSeconds(60f);
            minSpawnInterval -= 0.05f;
            maxSpawnInterval -= 0.1f;
            ShowEventOrWeapon();
        }

        CardsController.Instance.ClearCards();
        screenVictory.gameObject.SetActive(true);
    }

    private void ShowEventOrWeapon() {
        int random = Random.Range(0, 100);

        if (random <= 40) {
            CardsController.Instance.SpawnWeaponCards();
        } else {
            CardsController.Instance.SpawnEventCards();
        }
    }

    private IEnumerator cr_change_progress() {
        float currentTime = 0;

        while (slider.value != 1) {
            slider.value = currentTime / 600f;

            yield return new WaitForSeconds(1f);

            currentTime++;
        }
    }
}
