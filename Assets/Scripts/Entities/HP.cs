﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour {
    public bool isCanGetDmg = true;

    public int _hpPoints;
    public int hpPoints {
        set {
            if (isCanGetDmg) {
                StartCoroutine(cr_WaitBeforeNextDmg());

                _hpPoints = value;

                if (_hpPoints <= 0) {
                    GetComponent<Entity>().PlayDestroyEffect();

                    if (GetComponent<Enemy>() && GetComponent<Enemy>().currentWeapon != null) {
                        GetComponent<Enemy>().currentWeapon.isShooting = false;
                    }


                }
            }
        }
        get {
            return _hpPoints;
        }
    }

    public IEnumerator cr_WaitBeforeNextDmg() {
        isCanGetDmg = false;
        yield return new WaitForSeconds(2f);
        isCanGetDmg = true;
    }
}
